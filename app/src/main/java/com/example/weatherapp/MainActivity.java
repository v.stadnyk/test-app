package com.example.weatherapp;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    private EditText user_field;
    private Button main_btn;
    private TextView result_info;


      @Override
      protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        user_field = findViewById(R.id.user_field);
        main_btn = findViewById(R.id.main_btn);
        result_info = findViewById(R.id.result_info);

        main_btn.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 Log.i("!!!", "1");
                 if (user_field.getText().toString().trim().equals("")) {
                     Log.i("!!!", "2");
                    Toast.makeText(MainActivity.this, R.string.no_user_input, Toast.LENGTH_LONG).show();}
                else {
                     Log.i("!!!", "3");
                    String city = user_field.getText().toString();
                    String key = "91f9eeb36340f4ebc88ae47ff2ed9775";
                    String url = "https://api.openweathermap.org/data/2.5/weather?q=" + city + "&appid=" + key + "&units=metric";

                    new GetURLDate().execute(url);
                }

            }
        });
    }

    private class GetURLDate extends AsyncTask<String, String, String> {

        protected void onPreExecute() {
            super.onPreExecute();
            result_info.setText("Loading...");
            Log.i("!!!", "4");
        }

        @Override
        protected String doInBackground(String... strings) {
            Log.i("!!!", "5");
            HttpURLConnection connection = null;
            BufferedReader reader = null;


            try {
                URL url = new URL(strings[0]);
                Log.i("!!!", "5.1"+url.toString());
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));

                StringBuffer buffer = new StringBuffer();
                String line = "";

                while ((line = reader.readLine()) != null)
                    buffer.append(line).append("\n");
                Log.i("!!!", "5.2"+buffer.toString());
                return buffer.toString();


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();

            } finally {
                if (connection != null)
                    connection.disconnect();
                try {
                    if (reader != null) ;
                       reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            Log.i("!!!", "6");
            return null;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.i("!!!", "7"+result);
            result_info.setText(result);

        }

    }

}